#ifndef INPUTMANAGER_H_
#define INPUTMANAGER_H_

#define MAX_INPUT_EVENTS 50

typedef struct ArrowKeyPressedEventStruct {
	char arrow;
} ArrowKeyPressedEvent;

typedef struct InputQueueStruct {
	int startPos;
	int size;
	ArrowKeyPressedEvent** elements;
} InputQueue;

extern InputQueue inputQueue;

void InputManager_init();
void InputManager_pushEvent(char direction);
ArrowKeyPressedEvent* InputManager_popEvent();
int InputManager_isInputQueueEmpty();
int InputManager_isInputQueueFull();
void InputManager_free();

#endif /* INPUTMANAGER_H_ */

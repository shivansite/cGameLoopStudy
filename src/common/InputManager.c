#include <stdio.h>
#include <stdlib.h>

#include "InputManager.h"

InputQueue inputQueue;

void InputManager_init() {
	inputQueue.startPos = 0;
	inputQueue.size = 0;
	inputQueue.elements = malloc(MAX_INPUT_EVENTS*sizeof(ArrowKeyPressedEvent*));
	int i;
	for(i=0; i<MAX_INPUT_EVENTS; i++) {
		inputQueue.elements[i] = malloc(sizeof(ArrowKeyPressedEvent));
	}
}

void InputManager_pushEvent(char direction) {
	if(InputManager_isInputQueueFull()) {
		return;
	}
	int insertPos = (inputQueue.startPos + inputQueue.size) % MAX_INPUT_EVENTS;
	inputQueue.elements[insertPos]->arrow = direction;
	inputQueue.size++;
}

ArrowKeyPressedEvent* InputManager_popEvent() {
	if(InputManager_isInputQueueEmpty()) {
		return NULL;
	}
	ArrowKeyPressedEvent* event = inputQueue.elements[inputQueue.startPos % MAX_INPUT_EVENTS];
	inputQueue.startPos++;
	inputQueue.size--;
	return event;
}

int InputManager_isInputQueueEmpty() {
	if(inputQueue.size==0) {
		return 1;
	} else {
		return 0;
	}
}

int InputManager_isInputQueueFull() {
	if(inputQueue.size==MAX_INPUT_EVENTS) {
		return 1;
	} else {
		return 0;
	}
}

void InputManager_free() {
	int i;
	for(i=0; i<MAX_INPUT_EVENTS; i++) {
		free(inputQueue.elements[i]);
	}
	free(inputQueue.elements);
}

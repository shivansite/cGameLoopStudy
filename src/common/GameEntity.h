#ifndef GAMEENTITY_H_
#define GAMEENTITY_H_

#include <GL/gl.h>
#include "../common/InputManager.h"


extern GLfloat SQUARE_COLOR_RED[3];
extern GLfloat SQUARE_COLOR_BLUE[3];

typedef struct GameEntityStruct {

	float startX;
	float startY;
	float width;
	float height;
	int colorSwitchIntervalMs;
	GLfloat* currentColor;
	int stateUpdatesAccumulator;

	double updateStateGlobalStartTime;
	int updateStateGlobalCounter;
	double precision;

} GameEntity;

GameEntity* GameEntity_new(float startX, float startY, float width, float height, int colorSwitchIntervalMs);
void GameEntity_updateState(GameEntity* gameEntity, ArrowKeyPressedEvent* inputEvent);
void GameEntity_updateGraphics(GameEntity* gameEntity);
void GameEntity_updatePrecisionMeasurement(GameEntity* gameEntity);
void GameEntity_free(GameEntity* gameEntity);

#endif /* GAMEENTITY_H_ */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common/GameEntity.h"
#include "autoLoopingGameLoop/ALGameLoop.h"
#include "onDemandLoopingGameLoop/ODLGameLoop.h"


int main (int argc, char *argv[]) {
	int gameEntitiesLength = 4;
	GameEntity** gameEntities = malloc(gameEntitiesLength*sizeof(GameEntity*));
	gameEntities[0] = GameEntity_new(10,-130,50,50, 500);
	gameEntities[1] = GameEntity_new(430,-130,50,50, 500);
	gameEntities[2] = GameEntity_new(440,-230,50, 50, 2000);
	gameEntities[3] = GameEntity_new(10,-230,30,30, 2000);

	if(argc==2 && strcmp("odl",argv[1]) == 0) {
		ODLGameLoop_start(gameEntities, gameEntitiesLength);
	} else {
		ALGameLoop_start(gameEntities, gameEntitiesLength);
	}
	return 0;
}

#ifndef ODLGAMELOOP_H_
#define ODLGAMELOOP_H_

#include "../common/GameEntity.h"

void ODLGameLoop_start(GameEntity** gameEntitiesArr, int gameEntitiesArrLength);
void ODLGameLoop_free();

#endif /* ODLGAMELOOP_H_ */

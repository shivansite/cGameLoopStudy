#ifndef ALGAMELOOP_H_
#define ALGAMELOOP_H_

#include "../common/GameEntity.h"

void ALGameLoop_start(GameEntity** gameEntitiesArr, int gameEntitiesArrLength);
void ALGameLoop_free();

#endif /* ALGAMELOOP_H_ */

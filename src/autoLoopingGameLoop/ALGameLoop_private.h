#ifndef ALGAMELOOP_PRIVATE_H_
#define ALGAMELOOP_PRIVATE_H_

#include "../common/GameEntity.h"

typedef struct ALGameLoopStateStruct {
	GameEntity** gameEntities;
	int gameEntitiesLength;

	double lastLoopTime;
	double timeAccumulatedMs;

	int upsCount;
	int fpsCount;
	double lastMeasurementTime;

	double simulatedDelayDurationMs;
} ALGameLoopState;

extern ALGameLoopState alGameLoopState;

void ALGameLoop_initGameLoopState(GameEntity** gameEntities, int gameEntitiesLength);
void ALGameLoop_initOpenGL();
void ALGameLoop_onLoop();
void ALGameLoop_updateState();
void ALGameLoop_updateGraphics();
void ALGameLoop_onWindowReshape();
void ALGameLoop_onKeyboard(unsigned char key, int x, int y);
void ALGameLoop_updateMeasurements();


#endif /* ALGAMELOOP_PRIVATE_H_ */
